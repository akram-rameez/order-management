'use strict';

app.factory('orders', function($q, $timeout) {

    var orders = [];

    orders.getOrders = function() {

    	var deferred = $q.defer();

    	$timeout(function () {
    		deferred.notify('sending request');

	        deferred.resolve ({
	            'count': 9,
	            'data': [{
	                'id': 1,
	                'customer': 'abc2',
	                'type': 'Cancelled',
	                'product': 'def',
	            }, {
	                'id': 2,
	                'customer': 'abc1',
	                'type': 'Shipped',
	                'product': 'def',
	            }, {
	                'id': 3,
	                'customer': 'abc4',
	                'type': 'Confirmed',
	                'product': 'def',
	            }, {
	                'id': 4,
	                'customer': 'abc5',
	                'type': 'Shipped',
	                'product': 'def',
	            }, {
	                'id': 5,
	                'customer': 'abc67',
	                'type': 'Shipped',
	                'product': 'def',
	            }, {
	                'id': 6,
	                'customer': 'abc345',
	                'type': 'Cancelled',
	                'product': 'def',
	            }, {
	                'id': 6,
	                'customer': 'abcaa',
	                'type': 'Confirmed',
	                'product': 'def',
	            }, {
	                'id': 8,
	                'customer': 'baabc',
	                'type': 'Created',
	                'product': 'def',
	            }, {
	                'id': 9,
	                'customer': 'abcbaabc44',
	                'type': 'Created',
	                'product': 'def',
	            }]
	        });
    		
    	}, 2000);

    	return deferred.promise;

    }

    return orders;
});