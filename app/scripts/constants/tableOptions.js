'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
app.constant('tableOptions', {
    rowHeight: 50,
    headerHeight: 50,
    footerHeight: false,
    scrollbarV: false,
    checkboxSelection: true,
    selectable: true,
    multiSelect: true,
    columns: [{
        name: 'Name',
        prop: 'name',
        width: 300,
        isCheckboxColumn: true,
        headerCheckbox: true
    }, {
        name: 'Gender',
        prop: 'gender'
    }, {
        name: 'Company',
        prop: 'company'
    }]
});
