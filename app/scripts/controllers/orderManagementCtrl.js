'use strict';

app.controller('OrderManagementCtrl', function($mdEditDialog, $q, $scope, $timeout, orders, $mdToast) {

    $scope.selected = [];
    $scope.limitOptions = [5, 10, 15];

    $scope.options = {
        rowSelection: true,
        multiSelect: true,
        autoSelect: true,
        decapitate: false,
        largeEditDialog: false,
        boundaryLinks: false,
        limitSelect: false,
        pageSelect: false
    };

    $scope.query = {
        order: 'customer',
        limit: 5,
        page: 1
    };

    var last = {
      bottom: false,
      top: true,
      left: false,
      right: true
    };

    $scope.toastPosition = angular.extend({},last);

    function sanitizePosition() {
      var current = {}

      current.top = true;
      current.bottom = false;
      current.left = false;
      current.right = true;

      last = angular.extend({},current);
    }

    $scope.getToastPosition = function() {
      sanitizePosition();

      return Object.keys($scope.toastPosition)
        .filter(function(pos) { return $scope.toastPosition[pos]; })
        .join(' ');
    };

    var showSimpleToast = function(message) {
        var pinTo = $scope.getToastPosition();

        $mdToast.show(
            $mdToast.simple().textContent(message).position(pinTo).hideDelay(3000)
        );
    };

    $scope.toggleLimitOptions = function() {
        $scope.limitOptions = $scope.limitOptions ? undefined : [5, 10, 15];
    };

    $scope.getTypes = function() {
        return ['Created','Confirmed','Cancelled','Shipped'];
    };

    $scope.loadStuff = function() {
        $scope.promise = orders.getOrders();
        $scope.promise.then(function (data) {
          $scope.orders = data;
        });
    }

    $scope.logPagination = function(page, limit) {
        $scope.selected = [];
        console.log('page: ', page);
        console.log('limit: ', limit);
    }

    $scope.onPaginate = function(page, limit) {
        console.log('page changed')
        console.log('get data');
        //     console.log('page: ', page);
    }

    $scope.changeType = function(item) {
        console.log('item', item);
        console.log('save particular item');
        showSimpleToast('Order #'+item.id+' changed to '+item.type);
    }

    $scope.bulkOperation = function(operation) {
        showSimpleToast($scope.selected.length + ' fields marked as ' + operation);
        for (var i = 0; i < $scope.selected.length; i++) {
          $scope.selected[i].type = operation;
        }
        $scope.selected = [];
    }

    $scope.init = function() {

        $scope.promise = orders.getOrders();
        $scope.promise.then(function(data) {
            $scope.orders = data;
        });
    }
});
